#!/bin/bash

# Abort on error.
set -e

DISK_ID=$1

udisksctl unlock \
  -b "/dev/disk/by-uuid/$DISK_ID" \
  --key-file <(secret-tool lookup gvfs-luks-uuid "$DISK_ID" | tr -d '\n')
udisksctl mount -b "/dev/mapper/luks-$DISK_ID"
