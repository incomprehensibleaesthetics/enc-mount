# enc-mount

enc-mount is a command line utility for mounting an encrypted disk as a user from the command line.


## Dependencies

* bash
* secret-tool
* udisksctl


## Usage

```sh
enc-mount 726a8c1f-e8f2-4d59-91c5-a0b51b43cb5e # Replace with your disk id.
```
